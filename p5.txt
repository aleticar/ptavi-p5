***** Análisis de una sesión SIP

Se ha capturado una sesión SIP con Ekiga (archivo sip.cap.gz), que se puede abrir con Wireshark. Se pide rellenar las cuestiones que se plantean en este guión en el fichero p5.txt que encontrarás también en el repositorio.

  * Observa que las tramas capturadas corresponden a una sesión SIP con Ekiga, un cliente de VoIP para GNOME. Responde a las siguientes cuestiones:
    * ¿Cuántos paquetes componen la captura?
    
    [Se compone de 954 paquete]
    
    * ¿Cuánto tiempo dura la captura?
    
    [Dura 56.149 segundos]
    
    * ¿Qué IP tiene la máquina donde se ha efectuado la captura? ¿Se trata de una IP pública o de una IP privada? ¿Por qué lo sabes?
    
    [La IP dónde se ha lanzado la captura es 192.168.1.34, se trata de una IP privada de Clase C ya que está en el rango 192.168.0.0 a 192.168.255.255]

  * Antes de analizar las tramas, mira las estadísticas generales que aparecen en el menú de Statistics. En el apartado de jerarquía de protocolos (Protocol Hierarchy) se puede ver el porcentaje del tráfico correspondiente al protocolo TCP y UDP.
    * ¿Cuál de los dos es mayor? ¿Tiene esto sentido si estamos hablando de una aplicación que transmite en tiempo real?
    
    [ De UDP hay 96.2% de los paquetes mientras que de TCP hay solo 2.1%, tiene sentido por que si es una comunicación a tiempo real pues si hubiera mas mensajes TCP sería una comunicación mas lenta, ya que si se pierde algun paquete por el camino se tendría que esperar a que se reenviaran esos paquetes.]
    
    * ¿Qué otros protocolos podemos ver en la jerarquía de protocolos? ¿Cuales crees que son señal y cuales ruido?
    
    [Podemos ver IPV4 SIP RTP RTCP DNS HTTP ICMP ARP.
    Son señal :  UDP RTP SIP H.261
    Ruido sería : DNS ICMP IPV4 HTTP ARP]

  * Observa por encima el flujo de tramas en el menú de Statistics en IO Graphs. La captura que estamos viendo incluye desde la inicialización (registro) de la aplicación hasta su finalización, con una llamada entremedias.
    * Filtra por sip para conocer cuándo se envían paquetes SIP. ¿En qué segundos tienen lugar esos envíos?
    
    [Se envían entre 6-8 13-15 15-17 de 37-40 segundos.]
    
    * Y los paquetes con RTP, ¿cuándo se envían?
    
    [En el intervalor de 0-38 segundos se envían esos mensajes.]

  [Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados]

  * Analiza las dos primeras tramas de la captura.
    * ¿Qué servicio es el utilizado en estas tramas?
    
    [Utiliza el servicio de DNS]
    
    * ¿Cuál es la dirección IP del servidor de nombres del ordenador que ha lanzado Ekiga?
    
    [Ha lanzado con la direccion de 80.57.61.250]
    
    * ¿Qué dirección IP (de ekiga.net) devuelve el servicio de nombres?

	[Contesta con la direccion de ekiga 86.64.162.35]

  * A continuación, hay más de una docena de tramas TCP/HTTP.
    * ¿Podrías decir la URL que se está pidiendo?
    
    [Full request URI: http://ekiga.net/ip/]

    * ¿Qué user agent (UA) la está pidiendo?
    
    [User-Agent: Ekiga]

    * ¿Qué devuelve el servidor?
    
    [83.36.48.212]

    * Si lanzamos el navegador web, por ejemplo, Mozilla Firefox, y vamos a la misma URL, ¿qué recibimos? ¿Qué es, entonces, lo que está respondiendo el servidor?
    
    [Recibimos la IP del puesto en el que estemos trabajando, entonces nos responde con la dirección que se nos ve en internet.]

  * Hasta la trama 45 se puede observar una secuencia de tramas del protocolo STUN.
    * ¿Por qué se hace uso de este protocolo?
    
    [Porque permite conocer a los clientes las IP públicas]
    
    * ¿Podrías decir si estamos tras un NAT o no?
    
    [Si usamos STUN quiere decir que tenemos un NAT]

  * La trama 46 es la primera trama SIP. En un entorno como el de Internet, lo habitual es desconocer la dirección IP de la otra parte al realizar una llamada. Por eso, todo usuario registra su localización en un servidor Registrar. El Registrar guarda información sobre los usuarios en un servidor de localización que puede ser utilizado para localizar usuarios.
    * ¿Qué dirección IP tiene el servidor Registrar?
    [Destination: 86.64.162.35]

    * ¿A qué puerto (del servidor Registrar) se envían los paquetes SIP?
    [Destination Port: 5060]

    * ¿Qué método SIP utiliza el UA para registrarse?
    [Utiliza el metodo REGISTER]
    
    * Además de REGISTER, ¿podrías decir qué instrucciones SIP entiende el UA?
    [REGISTER INVITE SUBSCRIBE ACK BYE ]

  [Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados]

  * Fijémonos en las tramas siguientes a la número 46:
    * ¿Se registra con éxito en el primer intento?
    [No ya que en el paquete 50 devuelve un Status: 401 Unauthorized]
   
    * ¿Cómo sabemos si el registro se ha realizado correctamente o no?
    [Cuándo nos devuelve un 200 OK como es en el caso de paquete 54]
    
    * ¿Podrías identificar las diferencias entre el primer intento y el segundo de registro? (fíjate en el tamaño de los paquetes y mira a qué se debe el cambio)
    [Además de la diferencia del número de paquetes, vemos que en el primer intento falta la cabecera Authorization]
    
    * ¿Cuánto es el valor del tiempo de expiración de la sesión? Indica las unidades.
    [Expires: 3600 segundos]


  * Una vez registrados, podemos efectuar una llamada. Vamos a probar con el servicio de eco de Ekiga que nos permite comprobar si nos hemos conectado correctamente. El servicio de eco tiene la dirección sip:500@ekiga.net. Veamos el INVITE de cerca.
    * ¿Puede verse el nombre del que efectúa la llamada, así como su dirección SIP?
    [SIP Display info: "Gregorio Robles" y SIP from address: sip:grex@ekiga.net]


    * ¿Qué es lo que contiene el cuerpo de la trama? ¿En qué formato/protocolo está?
    [Contiene toda la información del Usuario que solicita el INVITE con el protocolo SDP]
    
    * ¿Tiene éxito el primer intento? ¿Cómo lo sabes?
    [No, ya que nos llega en el paquete 85 Status: 407 Proxy Authentication required]
    * ¿En qué se diferencia el segundo INVITE más abajo del primero? ¿A qué crees que se debe esto?
    [En el primer invite faltaba el proxy Authentication, en el segundo INVITE si que aparace esa cabecera]

  * Una vez conectado, estudia el intercambio de tramas.
    * ¿Qué protocolo(s) se utiliza(n)? ¿Para qué sirven estos protocolos?
    [Al principio hay un poco de ruido (DNS) y luego comienza con H.261 que es un estandar para comunicarse por vídeo y RTP porque es una conversación a tiempo real]
    
    * ¿Cuál es el tamaño de paquete de los mismos?
    [En el caso H.261 entre 1000-1100 normalmente menos algún caso de 200 300 y 400 mientras que  RTP se mantiene siempre en 214]
    * ¿Se utilizan bits de padding?
    [No ya que la cabecera de padding es : False]
    
    * ¿Cuál es la periodicidad de los paquetes (en origen; nota que la captura es en destino)?
    [El codificador al ser G.711 se envían en total 8000 muestras y como se envían aproximadamene 50 mensajes/segundo, entonces el periodo es 20 ms]
    
    * ¿Cuántos bits/segundo se envían?
    [Al ser G.711 nos permite enviar 64kbits/segundo]

  [Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados]

  * Vamos a ver más a fondo el intercambio RTP. En Telephony hay una opción RTP. Empecemos mirando los flujos RTP.
    * ¿Cuántos flujos hay? ¿por qué?
    [Hay dos flujos, para el audio que va en RTP y el video que va en H.261]
    * ¿Cuántos paquetes se pierden?
    [No se pierde ningún paquete ya que LOST=0]
    * ¿Cuál es el valor máximo del delta? ¿Y qué es lo que significa el valor de delta?
    [1290.44 ms, quiere decir que es lo máximo que va a tardar un paquete RTP]
    * ¿Cuáles son los valores de jitter (medio y máximo)? ¿Qué quiere decir eso? ¿Crees que estamos ante una conversación de calidad?
    [Son los valores que indican el retardo, no creo que estemos ante una comunicación de mucha calidad ya que el retardo puede variar mucho, si fuese constante pues probablemente sea de mejor calidad]
    

  * Elige un paquete RTP de audio. Analiza el flujo de audio en Telephony -> RTP -> Stream Analysis.
    * ¿Cuánto valen el delta y el jitter para el primer paquete que ha llegado?
    [ delta = 0 ms y  Jilter = 0 ms]
    
    * ¿Podemos saber si éste es el primer paquete que nos han enviado?
    [Si por la casilla marker]
    * Los valores de jitter son menores de 10ms hasta un paquete dado. ¿Cuál?
    [Hasta el paquete 246]
    * ¿A qué se debe el cambio tan brusco del jitter?
    [Como el valor de la delta cambia muchisimo, pues el jitter aumenta proporcionalmente]
    * ¿Es comparable el cambio en el valor de jitter con el del delta? ¿Cual es más grande?
    [Es mayor el valor de la delta]

  * En Telephony selecciona el menú VoIP calls. Verás que se lista la llamada de voz IP capturada en una ventana emergente. Selecciona esa llamada y pulsa el botón Play Streams.
    * ¿Cuánto dura la conversación?
    [Dura 21 segundos]
    * ¿Cuáles son sus SSRC? ¿Por qué hay varios SSRCs? ¿Hay CSRCs?
    [Para el audio SSRC=0xbf4afd37, para el video SSRC=0x43306582, y no hay CSRCs por que no se mezclan los flujos]

  * Identifica la trama donde se finaliza la conversación.
    * ¿Qué método SIP se utiliza?
    [Con el método BYE]
    * ¿En qué trama(s)?
    [El primer BYE se localiza en el paquete 924]
    * ¿Por qué crees que se envía varias veces?
    [Al estar usando UDP no tenemos confirmación de que hayan llegado al otro lado, por eso se envían varias ceces]

  * Finalmente, se cierra la aplicación de VozIP.
    * ¿Por qué aparece una instrucción SIP del tipo REGISTER?
    [Porque se ha querido dar de baja con expire:0]
    * ¿En qué trama sucede esto?
    [En el paquete 950]
    * ¿En qué se diferencia con la instrucción que se utilizó con anterioridad (al principio de la sesión)?
    [La diferencia es que en los primeros paquetes el usuario no estaba registrado por lo que envía el campo expire:3600 y en este caso se quiere dar de baja con expire:0]

  [Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados]

***** Captura de una sesión SIP

  * Dirígete a la web de Linphone (https://www.linphone.org/freesip/home) con el navegador y créate una cuenta SIP.  Recibirás un correo electrónico de confirmación en la dirección que has indicado al registrarte (mira en tu carpeta de spam si no es así).
  
  * Lanza linphone, y configúralo con los datos de la cuenta que te acabas de crear. Para ello, puedes ir al menú ``Ayuda'' y seleccionar ``Asistente de Configuración de Cuenta''. Al terminar, cierra completamente linphone.

  * Captura una sesión SIP de una conversación con el número SIP sip:music@sip.iptel.org. Recuerda que has de comenzar a capturar tramas antes de arrancar Ekiga para ver todo el proceso.

  * Observa las diferencias en el inicio de la conversación entre el entorno del laboratorio y el del ejercicio anterior:
    * ¿Se utilizan DNS y STUN? ¿Por qué?
    [Sí se utilizan las dos, STUN para buscar la IP privada de destino y el DNS para poder conectarse a Ekiga]
    * ¿Son diferentes el registro y la descripción de la sesión?
	[Solo cambia el nombre del sip, por lo demás son iguales.]
  * Identifica las diferencias existentes entre esta conversación y la conversación anterior:
    * ¿Cuántos flujos tenemos?
    [Son dos flujos de audio, uno de la dirección del puesto de laboratorio y el otro de la dirección a la que se ha llamado]
    * ¿Cuál es su periodicidad?
    [Aproximadamente se envían 50 paquetes/segundo]
    * ¿Cuánto es el valor máximo del delta y los valores medios y máximo del jitter?
    [De la delta 2238.60 ms y del jitter 6.47 ms]
    * ¿Podrías reproducir la conversación desde Wireshark? ¿Cómo? Comprueba que poniendo un valor demasiado pequeño para el buffer de jitter, la conversación puede no tener la calidad necesaria.
    [No he podido reproducirla porque a la hora de inentarlo con el voice call de wireshark salta:"RTP stream is empty or codec is unsupported"]
    * ¿Sabrías decir qué tipo de servicio ofrece sip:music@iptel.org?
    [Será una especie de servidor para reproducir música]

  [Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados]

  * Filtra por los paquetes SIP de la captura y guarda *únicamente* los paquetes SIP como p5.pcapng. Abre el fichero guardado para cerciorarte de que lo has hecho bien. Deberás añadirlo al repositorio.

[Al terminar la práctica, realiza un push para sincronizar tu repositorio GitLab]

IMPORTANTE: No olvides rellenar el test de la práctica 5 en el Aula Virtual de la asignatura.
